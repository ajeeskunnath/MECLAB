#include <wiringPi.h>
#include <stdio.h>
#include <time.h>
//#include <NewPing.h>
unsigned long rdPulseIn(int pin, int value, int timeout);
clock_t start,end;
double duration;
int distance;
#define trig 7 // pin number 7
#define echo 29 // pin number 40
int main()
{
if(wiringPiSetup()==-1)
{ 
printf("failed");	
}
printf("BEGIN \n");
	
pinMode(trig,OUTPUT);
pinMode(echo,INPUT);
while(1)
{
digitalWrite(trig,LOW);
delayMicroseconds(2);
digitalWrite(trig,HIGH);
delayMicroseconds(2);
digitalWrite(trig,LOW);
duration = rdPulseIn(echo, HIGH,1000);
distance=duration * 17150;
distance=distance/1000000;
if(distance>0 && distance <100)
{
printf("distance== %d cm \n ",distance);
}
}
return 0;
}

unsigned long rdPulseIn(int pin, int value, int timeout) { // the following comments assume that we're passing HIGH as value. timeout is in milliseconds

    unsigned long now = micros();
    while(digitalRead(pin) == value) { // wait if pin is already HIGH when the function is called, but timeout if it never goes LOW
        if (micros() - now > (timeout*1000)) {
            return 0;
        }
    }

    now = micros(); // could delete this line if you want only one timeout period from the start until the actual pulse width timing starts
    while (digitalRead(pin) != value) { // pin is LOW, wait for it to go HIGH befor we start timing, but timeout if it never goes HIGH within the timeout period
        if (micros() - now > (timeout*1000)) { 
            return 0;
        }
    }

    now = micros();
    while (digitalRead(pin) == value) { // start timing the HIGH pulse width, but time out if over timeout milliseconds
        if (micros() - now > (timeout*1000)) {
            return 0;
        }
    }
    return micros() - now;
}
